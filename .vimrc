syntax on
set number
set hlsearch
let myvar ="my vimrc loaded"
filetype indent on
set ai
set mouse=a
set incsearch
set confirm
set number
set ignorecase
set smartcase
set wildmenu
set wildmode=list:longest,full



" SML make code {{{
autocmd FileType sml setlocal makeprg=rlwrap\ mosml\ -P\ full\ '%'
" }}}

